# Vertical Axis Wind Turbine Design

The complete assembler-File of the turbine is:

# [urban-vawt-designentwurf.asm]

This repository contains the design files for a Vertical Axis Wind Turbine (VAWT). The aim of this project is to provide a comprehensive design that can be utilized for educational and research purposes. 

## Design Tool

The design files were created using Creo Parametric 9 with an educational license. Creo Parametric offers a range of powerful tools for geometry creation and modification, making it an ideal choice for complex projects like turbine design.

## Files

The repository includes various files necessary for the complete assembly of the turbine, including components and assembly files compatible with Creo Parametric 9.


## Contributing

Contributions to the project are welcome. 

## License

This project is shared under an educational license. It is intended for use by students, educators, and researchers. Please respect the licensing conditions.
